﻿<%@ Page Title="Ascension India Power Plant " Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="entry">
                        <div class="frame">
                            <img src="images/page/power-plant.jpg" />
                        </div>
                        <div class="divider">
                        </div>
                        <!-- one_fourth -->
                        <div>
                            <h4>
                                Power-Plant
                            </h4>
                            <p style="text-align: justify;">
                                Ascension India Industries Limited (AIIL) is coming with the biggest power plants
                                in India. India is the fifth largest electricity generation capacity in the world.
                                It has an installed capacity of over 152 gigawatts. AIIL will develop the capacity
                                of electricity for our country to make the position upgrades in the world. Detailed
                                project reports will have to be prepared on the basis of new modern technology.
                            </p>
                        </div>
                    </div>
                    <!-- entry-->
                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
