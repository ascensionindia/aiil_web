﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Text;
using System.IO;
public partial class _Contact_Us : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void submittedContact_Click(object sender, EventArgs e)
    {
        try
        {
            StringBuilder MailBody = new StringBuilder("<html><body><table>");
            MailBody.Append("<tr><td>Contact Name : </td><td>" + contactName.Text.Trim() + "</td></tr>");
            MailBody.Append("<tr><td>Email Id : </td><td>" + email.Text.Trim() + "</td></tr>");
            MailBody.Append("<tr><td>Phone No :  </td><td>" + contactPhone.Text.Trim() + "</td></tr>");
            MailBody.Append("<tr><td>Message :  </td><td>" + commentsText.Text.Trim() + "</td></tr>");
            MailBody.Append("</table></body></html>");
            SmtpClient smtp = new SmtpClient();
            MailMessage mail = new MailMessage();
            mail.To.Add("pchandra0312@gmail.com");
            mail.CC.Add(email.Text.Trim());
            mail.Subject = "AIIL Contact - " + contactName.Text;
            mail.Body = MailBody.ToString();
            //if (fuAttachment.HasFile)
            //{
            //    string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
            //    mail.Attachments.Add(new Attachment(fuAttachment.PostedFile.InputStream, FileName));
            //}
            smtp.EnableSsl = true;
            mail.IsBodyHtml = true;
            smtp.Send(mail);
            contactName.Text = string.Empty;
            email.Text = string.Empty;
            contactPhone.Text = string.Empty;
            commentsText.Text = string.Empty;
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Message Sucessfully Send...! \\n\\n We will contact you ASAP...!');", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message+ "');", true);
        }

    }
}
