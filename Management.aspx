﻿<%@ Page Title="Ascension India Management" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css"
        media="screen" />
    <script type='text/javascript' src='js/jquery.easing.js'></script>
    <script type='text/javascript' src='js/cufon-yui.js'></script>
    <script type='text/javascript' src='js/prettyPhoto/js/jquery.prettyPhoto.js'></script>
    <script type="text/javascript">/* <![CDATA[ */ document.write('<style type="text/css">.noscript { display:none; }</style>'); jQuery.preloadImages("images/ajax-loader.gif"); /* ]]> */</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="one_third">
                        <div class="full_portfolio_frame">
                            <div class="image_loader">
                                <div class="portfolio_full_img_holder">
                                    <div class="loading_gallery_full">
                                    </div>
                                    <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/management/ascension-india-cmd.jpg">
                                        <span class="rm_img noscript">
                                            <img src="images/management/ascension-india-cmd.jpg" alt="" /></span> <span class="roll_over rollover">
                                            </span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- one_half -->
                    <div class="two_third last">
                        <h4 class="portfolio">
                            <a href="#">Chairman</a><br />
                            <span class="date">November 01, 2012</span></h4>
                        <p>
                            The Mr.<strong> S.K. Singh</strong> is Chairman of Ascension India Industries Limited.
                            He is a responsible for full growth of this company and he is a very hard worker
                            and motivation person. His responsibilities in this company is as follows :-
                        </p>
                        <ul class="arrow_list">
                            <li>.....................................</li>
                            <li>..........................................................................</li>
                            <li>...............................................................................................................</li>
                        </ul>
                        <a class="button_link" href="#"><span>Read More</span></a>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <div class="one_third">
                        <div class="full_portfolio_frame">
                            <div class="image_loader">
                                <div class="portfolio_full_img_holder">
                                    <div class="loading_gallery_full">
                                    </div>
                                    <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/management/ascension-india-director1.jpg">
                                        <span class="rm_img noscript">
                                            <img src="images/management/ascension-india-director1.jpg" alt="" /></span> <span class="roll_over rollover">
                                            </span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- one_half -->
                    <div class="two_third last">
                        <h4 class="portfolio">
                            <a href="#">Director #: Mr. Suraj Kumar Ray</a><br />
                            <span class="date">May 01, 2013</span></h4>
                        <p>
                            Mr. Suraj Kumar Ray holding the posion of director in Ascension India Industries
                            Limited. He is a responsible persion and doing a well job for growth of this company
                            and he is a very hard worker and inovative person. Becouse of Mr. Ray company have
                            very good profite in diffrent areas also he has responsibilities in this company
                            is as follows :-
                        </p>
                        <ul class="arrow_list">
                            <li>.....................................</li>
                            <li>..........................................................................</li>
                            <li>...............................................................................................................</li>
                        </ul>
                        <a class="button_link" href="#"><span>Read More</span></a>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <div class="one_third">
                        <div class="full_portfolio_frame">
                            <div class="image_loader">
                                <div class="portfolio_full_img_holder">
                                    <div class="loading_gallery_full">
                                    </div>
                                    <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/management/ascension-india-director2.jpg">
                                        <span class="rm_img noscript">
                                            <img src="images/management/ascension-india-director2.jpg" alt="" /></span> <span class="roll_over rollover">
                                            </span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- one_half -->
                    <div class="two_third last">
                        <h4 class="portfolio">
                            <a href="#">Director # Mr. Sanjay Kumar Giri</a><br />
                            <span class="date">November 01, 2012</span></h4>
                        <p>
                            &nbsp;Mr. Sanjay Kumar Giri is of Ascension India Industries Limited. He is responsible
                            for full growth of this company and he is a very hard worker and motivation person.
                            His responsibilities in this company is as follows :-
                        </p>
                        <ul class="arrow_list">
                            <li>.....................................</li>
                            <li>..........................................................................</li>
                            <li>...............................................................................................................</li>
                        </ul>
                        <a class="button_link" href="#"><span>Read More</span></a>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
