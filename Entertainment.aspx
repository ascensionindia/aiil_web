﻿<%@ Page Title="Ascension India Entertainment" Language="C#" MasterPageFile="~/Site.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="entry">
                        <div class="frame">
                            <img src="images/page/entertainment.jpg" />
                        </div>
                        <div class="divider">
                        </div>
                        <!-- one_fourth -->
                        <div>
                            <h4>
                                Entertainment
                            </h4>
                            <p style="text-align: justify;">
                                Ascension India Industries Limited (AIIL) is coming with the digital age progresses,
                                consumer behavior is undergoing a dramatic transformation. Casting helps clients
                                in the media and entertainment industries take advantage of new customer relationship
                                opportunities in the digital world. Our global network of consultants, with proven
                                industry experience and drawn from leading business schools and universities, is
                                united by a passion for the communications industries.<br />
                                Arts and entertainment in India have a rich and ancient history. Right from ancient
                                times there has been a synthesis of indigenous and foreign influences that have
                                shaped the course of the arts of India, and consequently, the rest of Asia. Arts
                                refer to paintings, architecture, literature, music, dance, languages and cinema.
                                In early India, most of the arts were derived Vedic influences. After the birth
                                of contemporary Hinduism, Jainism, Buddhism, and Sikhism arts flourished under the
                                patronage of kings and emperors. The coming of Islam spawned a whole new era of
                                Indian architecture and art. Finally the British brought their own Gothic and Roman
                                influences and fused it with the Indian style. They have a culture infusion in their
                                art.
                            </p>
                        </div>
                    </div>
                    <!-- entry-->
                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
