﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Web.Routing" %>
<script RunAt="server">
    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        RegisterRoutes(RouteTable.Routes);
        //try
        //{
        //    System.IO.StreamReader sr = new System.IO.StreamReader(Server.MapPath("~/vistors.html"));
        //    string line;
        //    line = sr.ReadLine();
        //    sr.Close();
        //    sr.Dispose();
        //    Application["VisitorsCount"] = line;
        //}
        //catch (Exception ex)
        //{
        //    throw ex;
        //}
    }
    void RegisterRoutes(RouteCollection routes)
    {
        routes.MapPageRoute("Default",     // Route Name
            "index",                    // Url and Parameters
            "~/Default.aspx"           // Page Handling Request
            );
        routes.MapPageRoute("Agriculture", "Agriculture", "~/Agriculture.aspx");
        routes.MapPageRoute("Business_Locations", "Business-Locations", "~/Business_Locations.aspx");
        routes.MapPageRoute("Business_Opportunities", "Business-Opportunities", "~/Business_Opportunities.aspx");
        routes.MapPageRoute("Charitable_Trust", "Charitable-Trust", "~/Charitable_Trust.aspx");
        routes.MapPageRoute("Contacts", "Contacts", "~/Contacts.aspx");
        routes.MapPageRoute("Entertainment", "Entertainment", "~/Entertainment.aspx");
        routes.MapPageRoute("Gallery", "Gallery", "~/Gallery.aspx");
        routes.MapPageRoute("Mision_Vision", "Mision-Vision", "~/Mision_Vision.aspx");
        routes.MapPageRoute("Management", "Management", "~/Management.aspx");
        routes.MapPageRoute("Our_Team", "Our-Team", "~/Our_Team.aspx");
        routes.MapPageRoute("Power_Plant", "Power-Plant", "~/Power_Plant.aspx");
        routes.MapPageRoute("Travel_Tourism", "Travel-Tourism", "~/Travel_Tourism.aspx");
        routes.MapPageRoute("Water_Plant", "Water-Plant", "~/Water_Plant.aspx");
        routes.MapPageRoute("Ascension_India_Group", "Ascension-India-Group", "~/AscensionIndiaGroup.aspx");
        routes.MapPageRoute("News_Events", "News-Events", "~/News_Events.aspx");
        routes.MapPageRoute("NotFound", "{*Url}", "~/Error.aspx");

    }

    void Application_End(object sender, EventArgs e)
    {
        string path = HttpRuntime.AppDomainAppPath;
        System.IO.StreamWriter sw = new System.IO.StreamWriter(path + "/vistors.html");
        try
        {
           
            sw.WriteLine(Application["VisitorsCount"].ToString());
            sw.Close();
            sw.Dispose();
        }
        catch (Exception ex)
        {
            sw.WriteLine(ex.Message);
        }
    }

    void Application_Error(object sender, EventArgs e)
    {

        // Code that runs when an unhandled error occurs
        //Response.RedirectToRoute("Default");
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started
        Application["VisitorsCount"] = Convert.ToInt64(Application["VisitorsCount"]) + 1;
    }

    void Session_End(object sender, EventArgs e)
    {
        string path = HttpRuntime.AppDomainAppPath;
        System.IO.StreamWriter sw = new System.IO.StreamWriter(path + "/vistors.txt");
        try
        {

            sw.WriteLine(Application["VisitorsCount"].ToString());
            sw.Close();
            sw.Dispose();
        }
        catch (Exception ex)
        {
            sw.WriteLine(ex.Message);
        }
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
