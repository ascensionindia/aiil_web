﻿<%@ Page Title="Ascension India Mision-Vision" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="entry">
                        <div class="one_third">
                            <div class="frame">
                                <img src="images/page/mission-vision.jpg" width="260px" height="220px" /></div>
                        </div>
                        <!-- one_fourth -->
                        <div class="two_third">
                            <h4>
                                Mission & Vision
                            </h4>
                            <p style="text-align: justify;">
                                Ascension India Industries Limited (AIIL) is not only established to provide satisfactory
                                returns for its Investors/shareholders, but also to function as a peoples company,
                                whereby its employees are at all times motivated to perform a good job and to service
                                Clients to their full satisfaction.<br />
                                The main activities of Teamwork consist of ………………………,<br />
                                We are proud to mention that since the establishment of AIIL we are constantly looking
                                for new challenges and opportunities utilizing a professional and effective team
                                spirit and for those who have not worked with us yet, we would say: “Try Us Out”.
                                No matter what size of project, our aim is that every investor/stakeholders should
                                get the higher and reasonable return
                            </p>
                        </div>
                    </div>
                    <!-- entry-->
                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
