﻿<%@ Page Title="Ascension India Gallery" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="Gallery.aspx.cs" Inherits="_Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css"
        media="screen" />
    <script type='text/javascript' src='js/jquery.easing.js'></script>
    <script type='text/javascript' src='js/cufon-yui.js'></script>
    <script type='text/javascript' src='js/prettyPhoto/js/jquery.prettyPhoto.js'></script>
    <script type="text/javascript">/* <![CDATA[ */ document.write('<style type="text/css">.noscript { display:none; }</style>'); jQuery.preloadImages("images/ajax-loader.gif"); /* ]]> */</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
<div id="galary_call_to_action">
        <div class="inner"> 
                Ascension India Group is a social workers group and it's member's are working in
                different areas and doing well job for help financially poor people we have captured
                some memorable event done by ascension India’s members.
        </div> </div>
        <!-- inner -->
    <!-- breadcrumbs -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <asp:Literal ID="ltr_gallery" runat="server"></asp:Literal>
                </div>
            </div>
            <!-- content -->
        </div>
        <!-- primary_full -->
    </div>
    <!-- inner -->
</asp:Content>
