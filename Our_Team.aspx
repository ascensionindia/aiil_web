﻿<%@ Page Title="Ascension India Management" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css"
        media="screen" />
    <script type='text/javascript' src='js/jquery.easing.js'></script>
    <script type='text/javascript' src='js/cufon-yui.js'></script>
    <script type='text/javascript' src='js/prettyPhoto/js/jquery.prettyPhoto.js'></script>
    <script type="text/javascript">/* <![CDATA[ */ document.write('<style type="text/css">.noscript { display:none; }</style>'); jQuery.preloadImages("images/ajax-loader.gif"); /* ]]> */</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="one_third">
                        <div class="full_portfolio_frame">
                            <div class="image_loader">
                                <div class="portfolio_full_img_holder">
                                    <div class="loading_gallery_full">
                                    </div>
                                    <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/management/ascension-india-national-head-adviser.jpg">
                                        <span class="rm_img noscript">
                                            <img src="images/management/ascension-india-national-head-adviser.jpg" alt="" /></span>
                                        <span class="roll_over rollover"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- one_half -->
                    <div class="two_third last">
                        <h4 class="portfolio">
                            <a href="#">National Head Adviser</a><br />
                            <span class="date">November 01, 2013</span></h4>
                        <p>
                            The Mr.<strong> Ashok Rajbhar</strong> is State Head Adviser of Ascension India
                            Industries Limited. his Contact No : <b>9838600518</b>. His working area in this
                            company is as follows :-
                        </p>
                        <ul class="arrow_list">
                            <li>Contact for join a with company as a agent.</li>
                            <li>Contact for make bussiness with coompany.</li>
                        </ul>
                        <p>
                            &nbsp;</p>
                        <a class="button_link" href="#"><span>Read More</span></a>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="one_third">
                        <div class="full_portfolio_frame">
                            <div class="image_loader">
                                <div class="portfolio_full_img_holder">
                                    <div class="loading_gallery_full">
                                    </div>
                                    <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/management/ascension-india-state-head-adviser1.jpg">
                                        <span class="rm_img noscript">
                                            <img src="images/management/ascension-india-state-head-adviser1.jpg" alt="" /></span>
                                        <span class="roll_over rollover"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- one_half -->
                    <div class="two_third last">
                        <h4 class="portfolio">
                            <a href="#">State Head Adviser</a><br />
                            <span class="date">November 01, 2013</span></h4>
                        <p>
                            The Mr.<strong> Arbind Rajbhar</strong> is State Head Adviser of Ascension India
                            Industries Limited. his Contact No : <b>09628718683</b>. His working area in this
                            company is as follows :-
                        </p>
                        <ul class="arrow_list">
                            <li>Contact for join a with company as a agent.</li>
                            <li>Contact for make bussiness with coompany.</li>
                        </ul>
                        <p>
                            &nbsp;</p>
                        <a class="button_link" href="#"><span>Read More</span></a>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="one_third">
                        <div class="full_portfolio_frame">
                            <div class="image_loader">
                                <div class="portfolio_full_img_holder">
                                    <div class="loading_gallery_full">
                                    </div>
                                    <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/management/ascension-india-state-head-adviser2.jpg">
                                        <span class="rm_img noscript">
                                            <img src="images/management/ascension-india-state-head-adviser2.jpg" alt="" /></span>
                                        <span class="roll_over rollover"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- one_half -->
                    <div class="two_third last">
                        <h4 class="portfolio">
                            <a href="#">State Head Adviser</a><br />
                            <span class="date">November 01, 2013</span></h4>
                        <p>
                            The Mr.<strong> Ramlal Kushwaha</strong> is State Head Adviser of Ascension India
                            Industries Limited. his Contact No : <b>0000000000</b>. His working area in this
                            company is as follows :-
                        </p>
                        <ul class="arrow_list">
                            <li>Contact for join a with company as a agent.</li>
                            <li>Contact for make bussiness with coompany.</li>
                        </ul>
                        <p>
                            &nbsp;</p>
                        <a class="button_link" href="#"><span>Read More</span></a>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="one_third">
                        <div class="full_portfolio_frame">
                            <div class="image_loader">
                                <div class="portfolio_full_img_holder">
                                    <div class="loading_gallery_full">
                                    </div>
                                    <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/management/ascension-india-state-head-adviser3.jpg">
                                        <span class="rm_img noscript">
                                            <img src="images/management/ascension-india-state-head-adviser3.jpg" alt="" /></span>
                                        <span class="roll_over rollover"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- one_half -->
                    <div class="two_third last">
                        <h4 class="portfolio">
                            <a href="#">State Head Adviser</a><br />
                            <span class="date">November 01, 2013</span></h4>
                        <p>
                            The Mr.<strong> Anil Kumar Prajapati</strong> is State Head Adviser (UP) of Ascension
                            India Industries Limited. his Contact No : <b>+91-7398752575</b>. His working area
                            in this company is as follows :-
                        </p>
                        <ul class="arrow_list">
                            <li>Contact for join a with company as a agent.</li>
                            <li>Contact for make bussiness with coompany.</li>
                        </ul>
                        <p>
                            &nbsp;</p>
                        <a class="button_link" href="#"><span>Read More</span></a>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="one_third">
                        <div class="full_portfolio_frame">
                            <div class="image_loader">
                                <div class="portfolio_full_img_holder">
                                    <div class="loading_gallery_full">
                                    </div>
                                    <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/management/ascension-india-national-trainer.jpg">
                                        <span class="rm_img noscript">
                                            <img src="images/management/ascension-india-national-trainer.jpg" alt="" /></span>
                                        <span class="roll_over rollover"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- one_half -->
                    <div class="two_third last">
                        <h4 class="portfolio">
                            <a href="#">National Trainer</a><br />
                            <span class="date">November 01, 2013</span></h4>
                        <p>
                            The Mr.<strong> Sunil Kumar Singh</strong> is National Trainer of Ascension India
                            Industries Limited. his Contact No : <b>91-9576326108, +91-8795772697</b>. His working
                            area in this company is as follows :-
                        </p>
                        <ul class="arrow_list">
                            <li>Contact for any technical/functional training with software .</li>
                            <li>Contact for any agent downchain commisssion flow enquiry .</li>
                        </ul>
                        <p>
                            &nbsp;</p>
                        <a class="button_link" href="#"><span>Read More</span></a>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
            <div class="divider">
            </div>
            <div class="one_third">
                <div class="full_portfolio_frame">
                    <div class="image_loader">
                        <div class="portfolio_full_img_holder">
                            <div class="loading_gallery_full">
                            </div>
                            <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/team/ascension-india-jaunpur-branch.jpg">
                                <span class="rm_img noscript">
                                    <img src="images/team/ascension-india-jaunpur-branch.jpg" alt="" /></span> <span
                                        class="roll_over rollover"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- one_half -->
            <div class="two_third last">
                <h4 class="portfolio">
                    <a href="#">Jaunpur Branch Head</a><br />
                    <span class="date">November 01, 2012</span></h4>
                <p>
                    &nbsp;.............................. :-
                </p>
                <ul class="arrow_list">
                    <li>.....................................</li>
                    <li>..........................................................................</li>
                    <li>...............................................................................................................</li>
                </ul>
                <a class="button_link" href="#"><span>Read More</span></a>
            </div>
            <!-- one_half last -->
            <div class="clearboth">
            </div>
            <div class="divider">
            </div>
            <div class="one_third">
                <div class="full_portfolio_frame">
                    <div class="image_loader">
                        <div class="portfolio_full_img_holder">
                            <div class="loading_gallery_full">
                            </div>
                            <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/team/ascension-india-DMM-1.jpg">
                                <span class="rm_img noscript">
                                    <img src="images/team/ascension-india-DMM-1.jpg" alt="" /></span> <span class="roll_over rollover">
                                    </span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- one_half -->
            <div class="two_third last">
                <h4 class="portfolio">
                    <a href="#">DMM</a><br />
                    <span class="date">November 01, 2012</span></h4>
                <p>
                    The Mr.<strong> Suresh Prashad</strong> ................................... .............................................................
                    :-
                </p>
                <ul class="arrow_list">
                    <li>.....................................</li>
                    <li>..........................................................................</li>
                    <li>.................................................................... ............................................</li>
                </ul>
                <a class="button_link" href="#"><span>Read More</span></a>
            </div>
            <!-- one_half last -->
            <div class="clearboth">
            </div>
            <div class="divider">
            </div>
            <div class="one_third">
                <div class="full_portfolio_frame">
                    <div class="image_loader">
                        <div class="portfolio_full_img_holder">
                            <div class="loading_gallery_full">
                            </div>
                            <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/team/ascension-india-DMM-2.jpg">
                                <span class="rm_img noscript">
                                    <img src="images/team/ascension-india-DMM-2.jpg" alt="" /></span> <span class="roll_over rollover">
                                    </span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- one_half -->
            <div class="two_third last">
                <h4 class="portfolio">
                    <a href="#">Mr. Udayraj Madhesiya</a><br />
                    <span class="date">May 01, 2013</span></h4>
                <p>
                    ....................................... .................... :-
                </p>
                <ul class="arrow_list">
                    <li>.....................................</li>
                    <li>..........................................................................</li>
                    <li>...............................................................................................................</li>
                </ul>
                <a class="button_link" href="#"><span>Read More</span></a>
            </div>
            <!-- one_half last -->
            <div class="clearboth">
            </div>
            <div class="divider">
            </div>
            <div class="one_third">
                <div class="full_portfolio_frame">
                    <div class="image_loader">
                        <div class="portfolio_full_img_holder">
                            <div class="loading_gallery_full">
                            </div>
                            <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/team/ascension-india-DMM-3.jpg">
                                <span class="rm_img noscript">
                                    <img src="images/team/ascension-india-DMM-3.jpg" alt="" /></span> <span class="roll_over rollover">
                                    </span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- one_half -->
            <div class="two_third last">
                <h4 class="portfolio">
                    <a href="#">Mr. Nandlal Sony</a><br />
                    <span class="date">November 01, 2012</span></h4>
                <p>
                    &nbsp;.............................. :-
                </p>
                <ul class="arrow_list">
                    <li>.....................................</li>
                    <li>..........................................................................</li>
                    <li>...............................................................................................................</li>
                </ul>
                <a class="button_link" href="#"><span>Read More</span></a>
            </div>
            <!-- one_half last -->
            <div class="clearboth">
            </div>
            <div class="divider">
            </div>
            <div class="one_third">
                <div class="full_portfolio_frame">
                    <div class="image_loader">
                        <div class="portfolio_full_img_holder">
                            <div class="loading_gallery_full">
                            </div>
                            <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/team/ascension-india-RMM.jpg">
                                <span class="rm_img noscript">
                                    <img src="images/team/ascension-india-RMM.jpg" alt="" /></span> <span class="roll_over rollover">
                                    </span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- one_half -->
            <div class="two_third last">
                <h4 class="portfolio">
                    <a href="#">Mr. Arvind Prajapati</a><br />
                    <span class="date">November 01, 2012</span></h4>
                <p>
                    &nbsp;.............................. :-
                </p>
                <ul class="arrow_list">
                    <li>.....................................</li>
                    <li>..........................................................................</li>
                    <li>...............................................................................................................</li>
                </ul>
                <a class="button_link" href="#"><span>Read More</span></a>
            </div>
            <!-- one_half last -->
            <div class="clearboth">
            </div>
            <div class="divider">
            </div>
            <div class="one_third">
                <div class="full_portfolio_frame">
                    <div class="image_loader">
                        <div class="portfolio_full_img_holder">
                            <div class="loading_gallery_full">
                            </div>
                            <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/team/ascension-india-maharajganj-OP-head.jpg">
                                <span class="rm_img noscript">
                                    <img src="images/team/ascension-india-maharajganj-OP-head.jpg" alt="" /></span>
                                <span class="roll_over rollover"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- one_half -->
            <div class="two_third last">
                <h4 class="portfolio">
                    <a href="#">Mr. Sarvan Kumar Soni</a><br />
                    <span class="date">November 01, 2012</span></h4>
                <p>
                    &nbsp;.............................. :-
                </p>
                <ul class="arrow_list">
                    <li>.....................................</li>
                    <li>..........................................................................</li>
                    <li>...............................................................................................................</li>
                </ul>
                <a class="button_link" href="#"><span>Read More</span></a>
            </div>
            <!-- one_half last -->
            <div class="clearboth">
            </div>
            <div class="divider">
            </div>
            <div class="one_third">
                <div class="full_portfolio_frame">
                    <div class="image_loader">
                        <div class="portfolio_full_img_holder">
                            <div class="loading_gallery_full">
                            </div>
                            <a class="load_portfolio_img" rel="lightbox[portfolio]" href="images/team/ascension-india-.jpg">
                                <span class="rm_img noscript">
                                    <img src="images/team/ascension-india-.jpg" alt="" /></span> <span class="roll_over rollover">
                                    </span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- one_half -->
            <div class="two_third last">
                <h4 class="portfolio">
                    <a href="#">Mr. Anil Kumar Prajapti</a><br />
                    <span class="date">November 01, 2012</span></h4>
                <p>
                    &nbsp;.............................. :-
                </p>
                <ul class="arrow_list">
                    <li>.....................................</li>
                    <li>..........................................................................</li>
                    <li>...............................................................................................................</li>
                </ul>
                <a class="button_link" href="#"><span>Read More</span></a>
            </div>
            <!-- one_half last -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
