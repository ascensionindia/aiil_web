﻿<%@ Page Title="Ascension India Water Plant" Language="C#" MasterPageFile="~/Site.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="entry">
                        <div class="frame">
                            <img src="images/page/water-plant.jpg" />
                        </div>
                        <div class="divider">
                        </div>
                        <!-- one_fourth -->
                        <div>
                            <h4>
                                Water-Plant
                            </h4>
                            <p style="text-align: justify;">
                                Ascension India Industries Limited (AIIL) is coming with the biggest water plants
                                in India. Water purification is the process of removing undesirable chemicals, biological
                                contaminants, suspended solids and gases from contaminated water. The goal of this
                                process is to produce water fit for a specific purpose. Most water is disinfected
                                for human consumption (drinking water) but water purification may also be designed
                                for a variety of other purposes, including meeting the requirements of medical,
                                pharmacological, chemical and industrial applications. In general the methods used
                                include physical processes such as filtration, sedimentation, and distillation,
                                biological processes such as slow sand filters or biologically active carbon, chemical
                                processes such as flocculation and chlorination and the use of electromagnetic radiation
                                such as ultraviolet light.
                            </p>
                        </div>
                    </div>
                    <!-- entry-->
                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
