﻿<%@ Page Title="Ascension India Contacts" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="Contacts.aspx.cs" Inherits="_Contact_Us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="one_half">
                        <h3>
                            General Contact Info</h3>
                        <strong>Web site: www.ascensionindia.com</strong><br />
                        Email:contact@ascensionindia.com<br />
                        Phone:#1. (011)-6565-6011<br />
                        Phone:#2. (011)-6525-0601 <<br />
                        <strong>Sales Enquiry :-</strong> Email :sales@ascensionindia.com<br />
                        <strong>Official Enquiry :- </strong>Email :admin@ascensionindia.com
                        <div class="divider">
                        </div>
                        <h3>
                            Corporate Info</h3>
                        <p>
                            <strong>Head Office</strong><br />
                            <strong>ASCENSION INDIA INDUSTRIES LTD.</strong>
                            <br />
                            A-965 2ND FLOOR J.J COLONY PANKHA ROAD NEAR- UTTAM NAGAR METRO STATION (EAST)
                            <br />
                            OPP: MUTHOOT FINANCE NEW DELHI PIN CODE :- 110059
                            <br />
                            Phone:#1. (011)-6565-6011<br />
                            Phone:#2. (011)-6525-0601
                            <br />
                            FAX: (866) 123-0000</p>
                    </div>
                    <!-- one_half -->
                    <div class="one_half last">
                        <h5>
                            Send us a Messge</h5>
                        <a name="contact_"></a>
                        <form method="post" id="contact_form" runat="server">
                        <p>
                            <asp:TextBox ID="contactName" name="contactName" runat="server" TabIndex="1" MaxLength="30"
                                CssClass="requiredFieldContact textfield" Placeholder="Enter Your Name" required="true"></asp:TextBox>
                            <label for="contactName" class="textfield_label">
                                Name *</label></p>
                        <p>
                            <asp:TextBox ID="email" name="email" runat="server" TabIndex="2" MaxLength="30" CssClass="requiredFieldContact email textfield"
                                Placeholder="Enter Your Email" required="true"></asp:TextBox>
                            <label for="email" class="textfield_label">
                                Email *</label></p>
                        <p>
                            <asp:TextBox ID="contactPhone" name="contactPhone" runat="server" TabIndex="3" MaxLength="12"
                                CssClass="requiredFieldContact textfield" Placeholder="Enter Your Phone No" required="true"></asp:TextBox>
                            <label for="contactPhone" class="textfield_label">
                                Phone No *</label></p>
                        <p>
                            <asp:TextBox ID="commentsText" name="commentsText" runat="server" TextMode="MultiLine"
                                TabIndex="3" MaxLength="200" CssClass="requiredFieldContact textarea" Placeholder="Enter Your comments"
                                required="true"></asp:TextBox>
                        </p>
                        <p class="screenReader">
                            <label class="screenReader" for="checking">
                                do not enter anything in this field</label>
                            <input type="text" value="" class="screenReader" id="checking" name="checking" /></p>
                        <p class="loadingImg">
                        </p>
<%--                        <p>
                            <asp:FileUpload ID="fuAttachment" runat="server" />
                            <p>--%>
                                <asp:Button ID="submittedContact" name="submittedContact" runat="server" Text="Submit"
                                    CssClass="button" OnClick="submittedContact_Click" />
                            </p>
                            <p class="screenReader">
                                <input type="hidden" value="submit/submit.php" name="submitUrl" id="submitUrl" /></p>
                        </form>
                        <!-- fancy_box -->
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <div class="fancy_box">
                        <h4>
                            Head Office Location Map</h4>
                        <p>
                            <span class="frame">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28019.84728229155!2d77.07006346859457!3d28.61534545677268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04db756c4a85%3A0x4be141a5df1c!2sUttam+Nagar+East!5e0!3m2!1sen!2sin!4v1404620719875"
                                    width="910" height="340" frameborder="0" style="border: 0"></iframe>
                            </span>
                        </p>
                    </div>
                    <%-- Jaunpur Branch Address--%>
                    <div class="one_half">
                        <h3>
                            JAUNPUR Branch
                        </h3>
                        <p>
                            <strong>ASCENSION INDIA INDUSTRIES LTD.</strong><br />
                            4th floor city tower Near- wazid pur tiraha Jaunpur<br />
                            Uttar Pradesh Pin No.222002<br />
                            <strong>Mr. Shivpujan Morya (Oprator)</strong>
                            <br />
                            Phone No:- 09838619563<br />
                            Email ID :- shivpujanm0063@ascensionindia.com</p>
                    </div>
                    <!-- one_half -->
                    <div class="one_half last">
                        <span class="frame">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28019.84728229155!2d77.07006346859457!3d28.61534545677268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04db756c4a85%3A0x4be141a5df1c!2sUttam+Nagar+East!5e0!3m2!1sen!2sin!4v1404620719875"
                                width="460" height="200" frameborder="0" style="border: 0"></iframe>
                        </span>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <%--  AZAM GARH Branch Address--%>
                    <div class="one_half">
                        <h3>
                            AZAM GARH Branch
                        </h3>
                        <p>
                            <strong>ASCENSION INDIA INDUSTRIES LTD.</strong><br />
                            Krishna Couching Center Professors Colony Raido pur (Madaya)<br />
                            OPP: Hindustan office Azam Garh Pin No.276001<br />
                            <strong>Mr. Umesh Yadav (Oprator)</strong>
                            <br />
                            Phone No:- 08090456117<br />
                            Email ID :- umeshky0041@ascensionindia.com</p>
                    </div>
                    <!-- one_half -->
                    <div class="one_half last">
                        <span class="frame">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28019.84728229155!2d77.07006346859457!3d28.61534545677268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04db756c4a85%3A0x4be141a5df1c!2sUttam+Nagar+East!5e0!3m2!1sen!2sin!4v1404620719875"
                                width="460" height="200" frameborder="0" style="border: 0"></iframe>
                        </span>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <%-- KAPTAN GANJ Branch Address--%>
                    <div class="one_half">
                        <h3>
                            CAPTAN GANJ Branch
                        </h3>
                        <p>
                            <strong>ASCENSION INDIA INDUSTRIES LTD.</strong><br />
                            Caiptanganj (Subash Chowk) Near SAHARA INDIA (GOLDEN TAILOR 2nd Floor)<br />
                            DISSTT- KUSHINAGAR (U.P) PIN NO.274301<br />
                            <strong>Mr. Abhishek (Oprator)</strong>
                            <br />
                            Phone No:- 09616387948<br />
                            Email ID :- abhishekk0058@ascensionindia.com</p>
                    </div>
                    <!-- one_half -->
                    <div class="one_half last">
                        <span class="frame">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28019.84728229155!2d77.07006346859457!3d28.61534545677268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04db756c4a85%3A0x4be141a5df1c!2sUttam+Nagar+East!5e0!3m2!1sen!2sin!4v1404620719875"
                                width="460" height="200" frameborder="0" style="border: 0"></iframe>
                        </span>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <%-- MAIRWA  Branch Address--%>
                    <div class="one_half">
                        <h3>
                            MAIRWA Branch
                        </h3>
                        <p>
                            <strong>ASCENSION INDIA INDUSTRIES LTD.</strong><br />
                            VIIL_BAIKUNTH CHHAPAR PO.MAIRWA PS.MAIRWA DISTRIC-SIWAN(BIHAR)<br />
                            MAIRWA PIN NO. 841239<br />
                            <strong>Mr. SHASHIBHUSHAN (Oprator)</strong>
                            <br />
                            Phone No:- 08873051089<br />
                            Email ID :- shashib0035@ascensionindia.com</p>
                    </div>
                    <!-- one_half -->
                    <div class="one_half last">
                        <span class="frame">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28019.84728229155!2d77.07006346859457!3d28.61534545677268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04db756c4a85%3A0x4be141a5df1c!2sUttam+Nagar+East!5e0!3m2!1sen!2sin!4v1404620719875"
                                width="460" height="200" frameborder="0" style="border: 0"></iframe>
                        </span>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <%-- GHAZIPUR Branch Address--%>
                    <div class="one_half">
                        <h3>
                            GHAZIPUR Branch
                        </h3>
                        <p>
                            <strong>ASCENSION INDIA INDUSTRIES LTD.</strong><br />
                            BADI BAG GODA SHAHARI NEAR HOME GAURD OFFICE<br />
                            GHAZIPUR PIN CODE- 233001<br />
                            <strong>Mr. SANDEEP (Oprator)</strong>
                            <br />
                            Phone No:- 09044544760,09506083745<br />
                            Email ID :- sandeepbs0065@ascensionindia.com</p>
                    </div>
                    <!-- one_half -->
                    <div class="one_half last">
                        <span class="frame">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28019.84728229155!2d77.07006346859457!3d28.61534545677268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04db756c4a85%3A0x4be141a5df1c!2sUttam+Nagar+East!5e0!3m2!1sen!2sin!4v1404620719875"
                                width="460" height="200" frameborder="0" style="border: 0"></iframe>
                        </span>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <%-- KACHHAWA Branch Address--%>
                    <div class="one_half">
                        <h3>
                            KACHHAWA Branch
                        </h3>
                        <p>
                            <strong>ASCENSION INDIA INDUSTRIES LTD.</strong><br />
                            c/o RAJENDRA YADAV VILL. & PO.-BARAINI (KACHHAWA)<br />
                            Uttar Pradesh Pin No.231501<br />
                            <strong>Mr. NIRMAL (Oprator)</strong>
                            <br />
                            Phone No:- 09956026830,7388485189<br />
                            Email ID :- nirmalsg0057@ascensionindia.com</p>
                    </div>
                    <!-- one_half -->
                    <div class="one_half last">
                        <span class="frame">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28019.84728229155!2d77.07006346859457!3d28.61534545677268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04db756c4a85%3A0x4be141a5df1c!2sUttam+Nagar+East!5e0!3m2!1sen!2sin!4v1404620719875"
                                width="460" height="200" frameborder="0" style="border: 0"></iframe>
                        </span>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <%-- VARANASI  Branch Address--%>
                    <div class="one_half">
                        <h3>
                            VARANASI Branch
                        </h3>
                        <p>
                            <strong>ASCENSION INDIA INDUSTRIES LTD.</strong><br />
                            CSC ADDRESS PLOT NO. 3 GURUNANAK COMPLEX KAMALA NAGAR<br />
                            SIGRA PIN CODE NO. 221010 VARANASI<br />
                            <strong>Mr. Dhirendar Nath Tiwari (Oprator)</strong>
                            <br />
                            Phone No:- 07398578406<br />
                            Email ID :- dntiwary0015@ascensionindia.com</p>
                    </div>
                    <!-- one_half -->
                    <div class="one_half last">
                        <span class="frame">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28019.84728229155!2d77.07006346859457!3d28.61534545677268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04db756c4a85%3A0x4be141a5df1c!2sUttam+Nagar+East!5e0!3m2!1sen!2sin!4v1404620719875"
                                width="460" height="200" frameborder="0" style="border: 0"></iframe>
                        </span>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <%-- MAHARAJ GANJ  Branch Address--%>
                    <div class="one_half">
                        <h3>
                            MAHARAJ GANJ Branch
                        </h3>
                        <p>
                            <strong>ASCENSION INDIA INDUSTRIES LTD.</strong><br />
                            BRANCH MAHARAJ GANJ (UP) PIN CODE - 273303<br />
                            GORAKHPUR ROAD<br />
                            <strong>Mr. SHRAWAN(Oprator)</strong>
                            <br />
                            Phone No:- 09794612730<br />
                            Email ID :- sharwanku0028@ascensionindia.com</p>
                    </div>
                    <!-- one_half -->
                    <div class="one_half last">
                        <span class="frame">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28019.84728229155!2d77.07006346859457!3d28.61534545677268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04db756c4a85%3A0x4be141a5df1c!2sUttam+Nagar+East!5e0!3m2!1sen!2sin!4v1404620719875"
                                width="460" height="200" frameborder="0" style="border: 0"></iframe>
                        </span>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <%-- HATA   Branch Address--%>
                    <div class="one_half">
                        <h3>
                            HATA Branch</h3>
                        <p>
                            <strong>ASCENSION INDIA INDUSTRIES LTD.</strong><br />
                            HATA, NEAR SBI BANK, PO. - HATA,<br />
                            DISTRICT - KUSHINAGAR (UP) PIN NO. 274203<br />
                            <strong>Mr.ARVIND KUMAR (Oprator)</strong>
                            <br />
                            Phone No:- 08858200080<br />
                            Email ID :- arvindmd0058@ascensionindia.com</p>
                    </div>
                    <!-- one_half -->
                    <div class="one_half last">
                        <span class="frame">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28019.84728229155!2d77.07006346859457!3d28.61534545677268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04db756c4a85%3A0x4be141a5df1c!2sUttam+Nagar+East!5e0!3m2!1sen!2sin!4v1404620719875"
                                width="460" height="200" frameborder="0" style="border: 0"></iframe>
                        </span>
                    </div>
                    <!-- one_half last -->
                    <div class="clearboth">
                    </div>
                    <div class="divider">
                    </div>
                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
