﻿<%@ Page Title="Ascension India" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css"
        media="screen" />
    <script type='text/javascript' src='js/jquery.easing.js'></script>
    <script type='text/javascript' src='js/cufon-yui.js'></script>
    <script type='text/javascript' src='js/prettyPhoto/js/jquery.prettyPhoto.js'></script>
    <meta name="slider_speed" content="5000" />
    <script type="text/javascript">
        jQuery.preloadImages("styles/silver/home_feature.jpg", "styles/silver/stage.jpg", "styles/silver/header.jpg", "images/slider_inactive.png", "images/buttons.gif", "styles/silver/buttons.gif", "images/slider_active.png", "styles/silver/drop.png", "styles/silver/dropR.png", "styles/silver/drop_sub.png");
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="BodyContent">
    <%--    <div style="position: absolute; z-index: 15; font-size: 22px; background: Red; color: White;
        margin: 0 300px; width: 700px; border-radius: 5px;">
        <marquee scrollamount="5" onmouseover="stop();" onmouseout="start();"> सॉफ्टवेयर में लॉगिन के लिए नीचे <img src="images/icons/login_button.png" /> बटन पर क्लिक करें.</marquee>
    </div>--%>
    <div id="home_feature">
        <div class="background">
            <div class="inner">
                <div id="slider_thumbnails">
                    <div id="slider_preview_img">
                    </div>
                </div>
                <div id="loading_slider">
                </div>
                <div id="slider_img">
                    <!-- start homepage slider_cropped -->
                    <div class="image_loader slider_cropped">
                        <div class="slider_content">
                            <h2>
                            </h2>
                            <p>
                            </p>
                        </div>
                        <div class="slider_img_cropped">
                            <a href="#" class="load_slider_img"><span class="rm_img">
                                <img src="images/slides/ascension.jpg" alt="" /></span> </a>
                        </div>
                    </div>
                    <!-- end homepage slider_cropped -->
                    <!-- start homepage slider_cropped -->
                    <div class="image_loader slider_cropped">
                        <div class="slider_content">
                            <h2>
                                Water-Plant</h2>
                            <p>
                                Ascension India Industries Limited (AIIL) is coming with the biggest water plants
                                in India. India is the fifth largest water generation capacity in the world. Detailed
                                project reports will have to be prepared on the basis of new modern technology.</p>
                            <a class="button" id="A1" runat="server" href="<%$RouteUrl:RouteName=Water_Plant %>">
                                Read More</a>
                        </div>
                        <div class="slider_img_cropped">
                            <a href="#" class="load_slider_img"><span class="rm_img">
                                <img src="images/slides/water-plant.jpg" alt="" /></span> </a>
                        </div>
                    </div>
                    <!-- end homepage slider_cropped -->
                    <!-- start homepage slider_cropped -->
                    <div class="image_loader slider_cropped">
                        <div class="slider_content">
                            <h2>
                                Charetable Trust</h2>
                            <p>
                                Ascension India Industries Limited (AIIL) is coming with charitable trust, which
                                created for advancement of education, promotion of public health and too comfort,
                                relief of poverty, furtherance of religion or any other purpose regarded as charitable
                                in law.</p>
                            <a class="button" id="A4_4" runat="server" href="<%$RouteUrl:RouteName=Charitable_Trust %>">
                                Read More</a>
                        </div>
                        <div class="slider_img_cropped">
                            <a href="#" class="load_slider_img"><span class="rm_img">
                                <img src="images/slides/charetable.jpg" alt="" /></span> </a>
                        </div>
                    </div>
                    <!-- end homepage slider_cropped -->
                    <!-- start homepage slider_cropped -->
                    <div class="image_loader slider_cropped">
                        <div class="slider_content">
                            <h2>
                                Dairy farms</h2>
                            <p>
                                Ascension India Industries Limited is bringing together the strengths and talents
                                of businesses. We are creating the framework to achieve our ambition and become
                                a global leader in our business.</p>
                            <a class="button" href="#">Read More</a>
                        </div>
                        <div class="slider_img_cropped">
                            <a href="#" class="load_slider_img"><span class="rm_img">
                                <img src="images/slides/dairy.jpg" alt="" /></span> </a>
                        </div>
                    </div>
                    <!-- end homepage slider_cropped -->
                    <!-- start homepage slider_full_cropped -->
                    <div class="image_loader slider_full">
                        <div class="slider_content">
                            <h2>
                                Agriculture</h2>
                            <p>
                                It is the simplification of natures food webs and the gaining of energy for human
                                planting and animal consumption. AIIL is coming with new modern technology for agriculture.
                                Which develops the various types of products as corn, wheat, rice, bananas, soy
                                bean, vegetables etc.</p>
                            <a class="button" id="A4_5" runat="server" href="<%$RouteUrl:RouteName=Agriculture %>">
                                Read More</a>
                        </div>
                        <div class="slider_img_full_cropped">
                            <a href="#" class="load_slider_img"><span class="rm_img">
                                <img src="images/slides/farming.jpg" alt="" /></span> <span class="slider_frame">
                                </span></a>
                        </div>
                    </div>
                    <!-- end homepage slider_full_cropped -->
                    <!-- start homepage slider_cropped -->
                    <div class="image_loader slider_cropped">
                        <div class="slider_content">
                            <h2>
                                Hotel & Restaurant</h2>
                            <p>
                                Ascension India Industries Limited is bringing together the strengths and talents
                                of businesses. We are creating the framework to achieve our ambition and become
                                a global leader in our business.</p>
                            <a class="button" href="#">Read More</a>
                        </div>
                        <div class="slider_img_cropped">
                            <a href="#" class="load_slider_img"><span class="rm_img">
                                <img src="images/slides/hotel.jpg" alt="" /></span> </a>
                        </div>
                    </div>
                    <!-- end homepage slider_cropped -->
                    <!-- start homepage slider_cropped -->
                    <div class="image_loader slider_cropped">
                        <div class="slider_content">
                            <h2>
                                Power-Plant</h2>
                            <p>
                                Ascension India Industries Limited (AIIL) is coming with the biggest power plants
                                in India. India is the fifth largest electricity generation capacity in the world.
                                It has an installed capacity of over 152 gigawatts. AIIL will develop the capacity
                                of electricity for our country to make the position upgrades in the world. Detailed
                                project reports will have to be prepared on the basis of new modern technology.</p>
                            <a class="button" id="A4_6" runat="server" href="<%$RouteUrl:RouteName=Power_Plant %>">
                                Read More</a>
                        </div>
                        <div class="slider_img_cropped">
                            <a href="#" class="load_slider_img"><span class="rm_img">
                                <img src="images/slides/power-plant.jpg" alt="" /></span> </a>
                        </div>
                    </div>
                    <!-- end homepage slider_cropped -->
                    <!-- start homepage slider_cropped -->
                    <div class="image_loader slider_cropped">
                        <div class="slider_content">
                            <h2>
                                Travel and Tourism</h2>
                            <p>
                                Every kind of landscape is imaginable in India or out of India. An abundance of
                                mountain ranges and national parks provide a good opportunity for eco-tourism and
                                tracking by AIIL for everyone. AIIL can offer you a dazzling array of destinations
                                and experiences.
                            </p>
                            <a class="button" id="A4_2" runat="server" href="<%$RouteUrl:RouteName=Travel_Tourism %>">
                                Read More</a>
                        </div>
                        <div class="slider_img_cropped">
                            <a href="#" class="load_slider_img"><span class="rm_img">
                                <img src="images/slides/tour-travels.jpg" alt="" /></span> </a>
                        </div>
                    </div>
                    <!-- end homepage slider_cropped -->
                    <!-- start homepage slider_cropped -->
                    <div class="image_loader slider_cropped">
                        <div class="slider_content">
                            <h2>
                            </h2>
                            <p>
                            </p>
                        </div>
                        <div class="slider_img_cropped">
                            <a href="#" class="load_slider_img"><span class="rm_img">
                                <img src="images/slides/social-events.jpg" alt="" /></span> </a>
                        </div>
                    </div>
                    <!-- end homepage slider_cropped -->
                </div>
                <!-- end slider_img -->
            </div>
            <!-- inner -->
        </div>
        <!-- background -->
    </div>
    <!-- home_feature -->
    <%--    <div id="call_to_action">
        <div class="inner">
            
        </div>
        <!-- inner -->
    </div>
    <!-- call_to_action -->--%>
    <div class="clearboth">
    </div>
    <!-- body_block -->
    <div id="body_block">
        <%--<div class="inner">
            <div style="width: 20%; float: left; font-size:18px; font-weight:bold;">
                <img src="images/icons/new.jpg" height="20px" width="30px" />Special offer :</div>
            <div style="width: 80%; float: right;">
                <a id="A2" runat="server" href="<%$RouteUrl:RouteName=News_Events %>" title="News_Events">
                    <marquee scrollamount="5" onmouseover="stop();" onmouseout="start();" style="position: relative;
                        z-index: 15; font-size: 16px; font-weight: bold; color: Red; width: "> Offer for agents given by Ascension India for duration 13-July-20014 to 31-July-2014.</marquee>
                </a>
            </div>
        </div>--%>
        <div class="inner">
            <div id="call_to_action">
                <div class="three_fourth">
                    <p style="text-align: justify;">
                        &nbsp;&nbsp;&nbsp; Ascension India Industries Limited is bringing together the strengths
                        and talents of businesses. We are creating the framework to achieve our ambition
                        and become a global leader in our business. Close to our investors/customers in
                        markets, curious and attentive to their expectations and needs, we are continuing
                        our industrial journey through innovation, technological progress and entrepreneurial
                        spirit. Every employee’s enthusiasm, passion for excellence, will make a history.
                        I have complete confidence in our ability to succeed.</p>
                </div>
                <div id="one_fourth">
                    <a href="#" id="call_to_action_button"></a>
                </div>
                <div class="clearboth">
                </div>
            </div>
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="entry">
                        <h2>
                            Wellcome to our site!</h2>
                        <div>
                            <p style="text-align: justify;">
                                Ascension India Industries Limited (AIIL) Service is one stop destination for financial
                                & legal advice & services in all over India. We are providing services in the sector
                                of Banking, Forex, Accounting, Insurance, Investment, Tax Consultant, Mortgage Advisor,
                                Company Establishment, Real Estate, & Travel tourism. Already overcome their crawling
                                and started running in the fast track of business war fare with its two hundreds
                                of share holders, associates and with most skillful management authority. Ascension
                                India Industries Limited has taken a long agenda to eradicate the social curses
                                with high guaranteed return of monetary investments, providing purified packaged
                                drinking water, encouraging tourism in Eastern India, providing healthy food with
                                high calorific value at lower cost, spreading high quality education, creating green
                                natural environment and obviously promising a high employment in this competitive
                                environment.
                                
                            </p>
                            <div id="readmore">
                                </div>
                        </div>
                    </div>
                    <!-- entry-->
                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
