﻿<%@ Page Title="Ascension India News-Events" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
            text-align:center;
            font-size:16px;
        }
      
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" Runat="Server">
    <div id="galary_call_to_action">
        <div class="inner"> 
          <h1 style=" font-family:Imprint MT Shadow; font-size:16px; font-weight:bold;">Special offer for agents given by Ascension India for duration 13-July-20014 to 31-July-2014.</h1>
          <h1 style=" font-family:Imprint MT Shadow; font-size:18px; font-weight:bold; color:Blue;"> <a href="documents/ASCENSION%20INDIA.doc" target="_blank" > Download Document</a></h1>
        </div> </div>
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                
                    <table class="style1">
                        <tr>
                            <td width="20%">
                                &nbsp;</td>
                            <td width="20%">
                                <img alt=""src="images/ascension_logo.gif" width="100px" height="80px" /></td>
                            <td width="20%">
                              <h1 style=" font-family:Imprint MT Shadow; font-size:20px; font-weight:bold;">Ascension India</h1></td>
                            <td width="20%">
                                &nbsp;</td>
                        </tr>
                    </table>
                
                    <table class="style1">
                        <tr>

                            <th width="10%">
                                Rank </th>
                            <th width="20%">
                               Business / Agent </th>
                            <th width="20%">
                                Total Agents</th>
                            <th width="20%">
                                Total Business</th>
                            <th width="30%">
                               Prize Amount</th>
                        </tr>
                        <tr>
                            <td width="10%">
                                8</td>
                            <td width="20%">
                                5000</td>
                            <td width="20%">
                                5</td>
                            <td width="20%">
                                25000</td>
                            <td width="30%">
                                <strong>Bag</strong> (200 Rs/-)</td>
                        </tr>
                        <tr>
                            <td width="10%">
                                9</td>
                            <td width="20%">
                                8000</td>
                            <td width="20%">
                                5</td>
                            <td width="20%">
                                40000</td>
                            <td width="30%">
                                <strong>Wall watch</strong> (300 Rs/-)</td>
                        </tr>
                        <tr>
                            <td width="10%">
                                10</td>
                            <td width="20%">
                                10000</td>
                            <td width="20%">
                                7</td>
                            <td width="20%">
                                70000</td>
                            <td width="30%">
                                <strong>Standard Bag</strong> (500 Rs/-)</td>
                        </tr>
                        <tr>
                            <td width="10%">
                                11</td>
                            <td width="20%">
                                15000</td>
                            <td width="20%">
                                8</td>
                            <td width="20%">
                                120000</td>
                            <td width="30%">
                                <strong>Wrist watch</strong> (900 Rs/-)</td>
                        </tr>
                        <tr>
                            <td width="10%">
                                12</td>
                            <td width="20%">
                                20000</td>
                            <td width="20%">
                                10</td>
                            <td width="20%">
                                200000</td>
                            <td width="30%">
                                <strong>Dinner set</strong> (1200 Rs/-)</td>
                        </tr>
                        <tr>
                            <td width="10%">
                                13</td>
                            <td width="20%">
                                25000</td>
                            <td width="20%">
                                12</td>
                            <td width="20%">
                                300000</td>
                            <td width="30%">
                                <strong>Colors mobile</strong> (1500 Rs/-)</td>
                        </tr>
                        <tr>
                            <td width="10%">
                                14</td>
                            <td width="20%">
                                35000</td>
                            <td width="20%">
                                15</td>
                            <td width="20%">
                                525000</td>
                            <td width="30%">
                                <strong>Multimedia mobile</strong> (2500 Rs/-)</td>
                        </tr>
                        </table>
                    <!-- start body content -->

                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>

