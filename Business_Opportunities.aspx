﻿<%@ Page Title="Ascension India Business Opportunities" Language="C#" MasterPageFile="~/Site.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="entry">
                        <div class="frame">
                            <img src="images/page/business-opportunities.jpg" />
                        </div>
                        <div class="divider">
                        </div>
                        <!-- one_fourth -->
                        <div>
                            <h4>
                                Business-Opportunities
                            </h4>
                            <p style="text-align: justify;">
                                A simple analysis, taking into account the increasing population (See side box),
                                growing consumption and the shrinking agricultural land, shows that there is a very
                                lucrative market for Indian companies with products or technologies in the following
                                areas:
                                <br />
                                • Food & Beverages: food processing, food packaging, food warehouse and transport,
                                health drinks, etc.<br />
                                • Home based: home décor products, kitchenware essentials, bed and bath, etc.
                                <br />
                            • Consultancy Services: engineering, business development, product development,
                            security analysis, etc.
                        </div>
                        </p>
                    </div>
                </div>
                <!-- entry-->
                <!-- end body content -->
            </div>
            <!-- content -->
        </div>
        <!-- primary-full -->
        <div class="clearboth">
        </div>
    </div>
    <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
