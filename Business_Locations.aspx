﻿<%@ Page Title="Ascension India Business Locations" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="entry">
                        <h4>
                            Business-Location
                        </h4>
                        <div class="frame">
                            <img src="images/page/business-location.jpg" />
                        </div>
                        <div class="divider">
                        </div>
                    </div>
                </div>
                <!-- entry-->
                <!-- end body content -->
            </div>
            <!-- content -->
        </div>
        <!-- primary-full -->
        <div class="clearboth">
        </div>
    </div>
    <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
