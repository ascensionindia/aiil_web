﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml;

public partial class _Gallery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StringBuilder sb = new StringBuilder();
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(Server.MapPath("~/Gallery.xml"));
            XmlNode gallery = xmldoc.GetElementsByTagName("gallery")[0];
            XmlNodeList gallerylist = gallery.ChildNodes;
            if (gallerylist.Count > 0)
            {
                int j = 0;               
                for (int i = 0; i < gallery.ChildNodes.Count; i++)
                {
                    j = i % 3;
                    if (j == 0)
                    {
                        sb.Append("<div class=one_third last> <div class=image_loader> <div class=portfolio_img_holder><div class=loading_gallery></div>");
                        sb.Append("<a class=load_portfolio_img rel=lightbox[portfolio] href='images/gallery/" + gallerylist[i].Attributes["img"].Value + "'>");
                        sb.Append("<span class=rm_img noscript><img src='images/gallery/" + gallerylist[i].Attributes["img"].Value + "'/></span><span class=roll_over rollover></span></a></div>");
                        sb.Append("<h3 class=portfolio><a href='#'>" + gallerylist[i].Attributes["title"].Value + "</a></h3></div></div>");
                    }
                    else
                    {
                        sb.Append("<div class=one_third> <div class=image_loader> <div class=portfolio_img_holder><div class=loading_gallery></div>");
                        sb.Append("<a class=load_portfolio_img rel=lightbox[portfolio] href='images/gallery/" + gallerylist[i].Attributes["img"].Value + "'>");
                        sb.Append("<span class=rm_img noscript><img src='images/gallery/" + gallerylist[i].Attributes["img"].Value + "'/></span><span class=roll_over rollover></span></a></div>");
                        sb.Append("<h3 class=portfolio><a href='#'>" + gallerylist[i].Attributes["title"].Value + "</a></h3></div></div>");
                    }
                }
                ltr_gallery.Text = sb.ToString();
            }
        }
    }
}