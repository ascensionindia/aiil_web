﻿<%@ Page Title="Ascension India Charitable Trust" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="entry">
                        <div class="frame">
                            <img src="images/page/charitable-trust.jpg" />
                        </div>
                        <div class="divider">
                        </div>
                        <!-- one_fourth -->
                        <div>
                            <h4>
                                Charitable & Trust
                            </h4>
                            <p style="text-align: justify;">
                                Ascension India Industries Limited (AIIL) is coming with charitable trust, which
                                created for advancement of education, promotion of public health and too comfort,
                                relief of poverty, furtherance of religion or any other purpose regarded as charitable
                                in law.<br />
                                Where the purpose of a charitable trust by AIIL, becomes impossible to carry out
                                then, under the legal doctrine of AIIL the trustees acting by a majority may choose
                                another charitable purpose as nearly like the original purpose as possible. In other
                                words, we can say that AIIL will give direct relief of poverty.
                            </p>
                        </div>
                    </div>
                    <!-- entry-->
                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
