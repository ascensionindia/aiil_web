﻿<%@ Page Title="Ascension India Agriculture" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="entry">
                        <div class="frame">
                            <img src="images/page/agriculture.jpg" />
                        </div>
                        <div class="divider">
                        </div>
                        <!-- one_fourth -->
                        <div>
                            <h4>
                                Agriculture
                            </h4>
                            <p style="text-align: justify;">
                                It is the simplification of natures food webs and the gaining of energy for human
                                planting and animal consumption. AIIL is coming with new modern technology for agriculture.
                                Which develops the various types of products as corn, wheat, rice, bananas, soy
                                bean, vegetables etc.
                            </p>
                        </div>
                    </div>
                    <!-- entry-->
                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
