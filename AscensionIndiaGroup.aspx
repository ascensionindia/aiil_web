﻿<%@ Page Title="Ascension India Group" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <form id="form1" runat="server">
    <div id="galary_call_to_action">
        <div class="inner">
            Ascension India Group is a group of company and ascension India Industries is the
            mother company of all company. Different child company are there we have given some
            link of important URL which is used by different office member or follower thank
            you...!
        </div>
    </div>
    <!-- inner -->
    <!-- breadcrumbs -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                 <div class="one_half" >
                        <h3 class="portfolio" style="text-align:left;">
                            <a href="http://www.ascensionmutual.com/" title="Ascension India Mutual Benefit"> Ascension India Mutual Benefit</a></h3>
                        <div class="clearboth">
                        </div>
                    </div>
                    <div class="one_third">
                        <a href="http://www.ascensionmutual.com/" title="Ascension India Mutual Benefit">
                            <img src="images/icons/aimbl_link.png" /></a>
                        <div class="clearboth">
                        </div>
                    </div>
                     <div class="one_half">
                        <h3 class="portfolio" style="text-align:left;">
                            <a href="http://www.aimbl.com/" title="Ascension India Mutual Benefit Software"> Ascension India Mutual Benefit Software</a></h3>
                        <div class="clearboth">
                        </div>
                    </div>
                    <div class="one_third">
                        <a href="http://www.aimbl.com/" title="Ascension India Mutual Benefit Software">
                            <img src="images/icons/aimbl_soft_link.png" /></a>
                        <div class="clearboth">
                        </div>
                    </div>
                    <div class="one_half">
                        <h3 class="portfolio" style="text-align:left;">
                            <a href="http://www.ascensionindialtd.com/" title="Ascension India Software"> Ascension India Software</a></h3>
                        <div class="clearboth">
                        </div>
                    </div>
                    <div class="one_third">
                        <a href="http://www.ascensionindialtd.com/" title="Ascension India Software">
                            <img src="images/icons/aiil_link.png" /></a>
                        <div class="clearboth">
                        </div>
                    </div>
                    <div class="one_half">
                        <h3 class="portfolio" style="text-align:left;">
                            <a href="https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=12&ct=1398389598&rver=6.4.6456.0&wp=SAPI_LONG&wreply=https:%2F%2Fdomains.live.com&lc=1033&id=66055"
                                title="Web Mail Account">Ascension India Web mail Account</a></h3>
                        <div class="clearboth">
                        </div>
                    </div>
                    <div class="one_third">
                        <a href="https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=12&ct=1398389598&rver=6.4.6456.0&wp=SAPI_LONG&wreply=https:%2F%2Fdomains.live.com&lc=1033&id=66055"
                            title="Web Mail Account">
                            <img src="images/icons/webmail_login.png" /></a>
                        <div class="clearboth">
                        </div>
                    </div>
                </div>
                <div class="clearboth">
                </div>
            </div>
        </div>
        <!-- content -->
    </div>
    <!-- primary_full -->
    </div>
    <!-- inner -->
    </form>
</asp:Content>
