﻿<%@ Page Title="Ascension India Traval Tourism" Language="C#" MasterPageFile="~/Site.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <!-- body_block -->
    <div id="body_block">
        <div class="inner">
            <div id="primary_full">
                <div class="content">
                    <!-- start body content -->
                    <div class="entry">
                        <div class="frame">
                            <img src="images/page/tour-travels.jpg" />
                        </div>
                        <div class="divider">
                        </div>
                        <!-- one_fourth -->
                        <div>
                            <h4>
                                Travel & Tourism
                            </h4>
                            <p style="text-align: justify;">
                                Every kind of landscape is imaginable in India or out of India. An abundance of
                                mountain ranges and national parks provide a good opportunity for eco-tourism and
                                tracking by AIIL for everyone. AIIL can offer you a dazzling array of destinations
                                and experiences. It offers you in summer, the subcontinent is sizzling, there are
                                spectacular retreats amidst the best beauty of Himalayas as well as the lush heights
                                of the western Ghats with cool tracking trails.<br />
                                In cool AIIL can offer various types of cities come alive with cultural feasts of
                                music and dance. There is an ideal time for you to go century hopping in romantic
                                cities studded medieval forts and places.Delight in the grace of a dancer or shop
                                till you drop buying exquisite skills, carved figurines, brass and silver ware,
                                finely crafted jewelry, miniature paintings, carpets………at unbelievable prices.<br />
                                As you can say that AIIL travel & Tourism is priceless for us apart from all.
                            </p>
                        </div>
                    </div>
                    <!-- entry-->
                    <!-- end body content -->
                </div>
                <!-- content -->
            </div>
            <!-- primary-full -->
            <div class="clearboth">
            </div>
        </div>
        <!-- inner -->
    </div>
    <!-- body_block -->
</asp:Content>
